/*
Magnetic Rack based on design for BOMB microtube rack by Tomasz Jurkowsk
https://bomb.bio/protocols/
https://www.thingiverse.com/thing:3199242
Copyright 2020  - Open Bioeconomy Lab
Software released under MIT License
Hardware released under CERN Open Hardware License v1.2
*/

/*
Set the parameters below to customise your rack then scroll to the bottom to render the different modules.
*/

// Set number of tubes 
tubes = 1;

/// COMPONENT PARAMETERS - ADJUST FOR YOUR MAGNETS AND TUBES

// Set width, height and depth of your magnets
magnet_d = 3;
magnet_h = 25;
magnet_w = 8;

// Set radius and length of "straight" top part of microcentrifuge tube
top_tube_radius = 5.9;
top_tube_length = 25;

// Set length and top and bottom radius of bottom, tapered part of  microcentrifuge tube
btm_tube_topradius = 5.9;
btm_tube_btmradius = 2.9;
btm_tube_length = 17.9;

// Set upper (ud) and lower (ld) radius of hole for tube
tube_ud = top_tube_radius+0.25;
tube_ld = btm_tube_btmradius;

/// FRAME PARAMETERS - 

// Set depth, height, width and thickness of module frame

frame_d = 20;
frame_h = 35;
frame_w = 20;
frame_t = 5;
side_frame_d = (frame_d/2)+(btm_tube_topradius+0.25-frame_t);



// Set depth, height and width of slope to get magnet closer to tube
wedge_angle = atan(btm_tube_length/(btm_tube_topradius-btm_tube_btmradius));
wedge_d =  (btm_tube_topradius-btm_tube_btmradius);
wedge_h = tan(wedge_angle)*wedge_d;
wedge_w = frame_w;

//Calculate point on hypothesis for placing the magnet recess
distance = (wedge_h*wedge_d)/(sqrt(pow(wedge_h,2))+(pow(wedge_d,2)));
angle = wedge_angle;
x = distance*cos(angle);
y = distance*sin(angle);
y_dist = (wedge_h-y)/tan(wedge_angle);

/// BEGIN MODEL

// ONLY UNCOMMENT MICROCENTRIFUGE MODEL FOR TESTING PURPOSES
//module model(){
//
//    translate ([frame_w/2,((frame_d+frame_t)/2),(top_tube_length-2)+frame_t])
//union(){
//    cylinder(top_tube_length,top_tube_radius,top_tube_radius);
//        translate([0,0,-btm_tube_length])
//    cylinder(btm_tube_length,btm_tube_btmradius,btm_tube_topradius);
//    };}
//    
module tube(){  

// Bottom frame, removing cylinder for holding bottoms of tubes
    difference(){
        cube([frame_w,frame_d,frame_t]);
            translate([frame_w/2,((frame_d+wedge_d)/2),frame_t])
            cylinder((frame_t),r1=tube_ld,r2=tube_ld,center=true);
        };

//top frame with tube hole

    difference(){
        translate([0, -frame_t, frame_h])
        cube([frame_w,frame_d+frame_t,frame_t]);
            translate([frame_w/2,((frame_d+frame_t)/2),frame_h])
            cylinder((2*frame_t),r1=tube_ud,r2=tube_ud,center=true);
        };
    
//// wedge with magnet holes
        
///magnet holes
        
difference(){
    union(){
    //generate wedge
        translate([frame_w,((frame_d+frame_t)/2-(btm_tube_topradius+0.5)),frame_t])
        rotate(a=[0,-90,0])
            linear_extrude(height = wedge_w, center = false, convexity = 0, twist = 0)
            polygon(points=[[0,0],[wedge_h,0],[0,wedge_d]], paths=[[0,2,1]]);
    // generate side frame
        translate ([0,((frame_d+frame_t)/2-(btm_tube_topradius+0.5)),0])
        rotate ([90,0,0]) 
        cube([frame_w,frame_h,side_frame_d]);
        };
        
// Need to calculate y translation better
    translate([(wedge_w/2),frame_t+y_dist-1,x+(magnet_h/2+frame_t+1)])
    rotate([180-wedge_angle,0,0])
        cube(size=[magnet_w,magnet_h,magnet_d],center=true);     

};
};

model();

    
// Add multiple tube modules together

module multitube(){
    for (i=[0:tubes-1])
        translate([i*frame_w,0,0])
        tube();
};

// Mirror the tube module to make a double-sided version

module doublesided(){
    multitube(); // original
    mirror([0,1,0]) 
    multitube();
};

// Render the tube rack, if you want a single-sided design, comment out "doublesided();" by adding with two forward slashes at the start of that line

multitube();
//doublesided();


